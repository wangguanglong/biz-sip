package com.bizmda.bizsip.source.api;

import java.lang.reflect.Proxy;

/**
 * Source层获取App调用接口
 * @param <T> App服务的接口类
 */
public class SourceClientFactory<T> {
    /**
     * 获取App服务调用接口
     * @param tClass App服务的接口类（要求是interface）
     * @param appServiceId 应用服务ID
     * @param <T> 接口类泛型
     * @return 接口调用句柄
     */
    public static <T> T getAppServiceClient(Class<T> tClass, String appServiceId) {
        final AppServiceClientProxy<T> appServiceClientProxy = new AppServiceClientProxy<>(tClass,appServiceId);
        return (T) Proxy.newProxyInstance(tClass.getClassLoader(), new Class[]{tClass}, appServiceClientProxy);
    }
}
