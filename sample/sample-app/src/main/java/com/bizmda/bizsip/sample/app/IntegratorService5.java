package com.bizmda.bizsip.sample.app;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.AppBeanInterface;
import com.bizmda.bizsip.app.api.AppClientFactory;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.sample.sink.api.SinkInterface1;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class IntegratorService5 implements AppBeanInterface {
    private SinkInterface1 delayServiceClient = AppClientFactory
            .getDelayAppServiceClient(SinkInterface1.class,"/springbean",
            1000,2000,4000,8000,16000,32000);

    @Override
    public JSONObject process(JSONObject message) throws BizException {
        int maxRetryNum = (int)message.get("maxRetryNum");
        String result = (String)message.get("result");
        String result1 = this.delayServiceClient.notify(maxRetryNum,result);
        log.info("成功返回: {}",result1);
        return message;
    }
}
