package com.bizmda.bizsip.sample.app;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.AppBeanInterface;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessageInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.bizmda.bizsip.app.api.AppClientFactory;

@Slf4j
@Service
public class IntegratorService2 implements AppBeanInterface {
    private BizMessageInterface delayBizServiceClient = AppClientFactory
            .getDelayAppServiceClient(BizMessageInterface.class,"/openapi/safservice1",1000,2000,4000);

    @Override
    public JSONObject process(JSONObject message) throws BizException {
        this.delayBizServiceClient.call(message);
        return message;
    }
}
