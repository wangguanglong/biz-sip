package com.bizmda.bizsip.sample.sink.controller;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.sink.api.SinkBeanInterface;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 史正烨
 */
@Service
public class ActServer implements SinkBeanInterface {
    private static final Map<String,Integer> ID_BALANCE_MAP = new HashMap<>();
    static {
        ID_BALANCE_MAP.put("003",300);
        ID_BALANCE_MAP.put("004",400);
        ID_BALANCE_MAP.put("005",500);
    }

    @Override
    public JSONObject process(JSONObject jsonObject) throws BizException {
        String accountNo = (String)jsonObject.get("accountNo");
        Integer balance = ID_BALANCE_MAP.get(accountNo);
        if (balance == null) {
            throw new BizException(100,"账户不存在!");
        }
        jsonObject.set("balance",balance);
        return jsonObject;
    }
}
