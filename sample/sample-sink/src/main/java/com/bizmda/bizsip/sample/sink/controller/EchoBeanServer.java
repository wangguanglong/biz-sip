package com.bizmda.bizsip.sample.sink.controller;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizUtils;
import com.bizmda.bizsip.sink.api.SinkBeanInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 史正烨
 */
@Slf4j
@Service
public class EchoBeanServer implements SinkBeanInterface {
    @Override
    public JSONObject process(JSONObject inMessage) throws BizException {
        log.debug("EchoBeanServer传入消息:\n{}", BizUtils.buildJsonLog(inMessage));
        return inMessage;
    }
}
