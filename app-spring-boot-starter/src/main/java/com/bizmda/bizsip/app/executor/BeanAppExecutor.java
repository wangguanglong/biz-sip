package com.bizmda.bizsip.app.executor;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.IntegratorBeanInterface;
import com.bizmda.bizsip.common.*;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Map;

@Slf4j
public class BeanAppExecutor extends AbstractAppExecutor {
    private IntegratorBeanInterface javaIntegratorService;
    private Class clazz;
    private Object springBean = null;

    public BeanAppExecutor(String serviceId, String type, Map configMap,boolean isAppYml) {
        super(serviceId, type, configMap);
        String className = (String)(isAppYml?
                configMap.get("class-name"):configMap.get("className"));
        try {
            this.clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            log.error("创建"+className+"类失败！",e);
            return;
        }
    }

    @Override
    public void init() {
        // 没有初始化内容
    }

    @Override
    public BizMessage<JSONObject> doAppService(BizMessage<JSONObject> message) throws BizException {
        log.debug("入参:\n{}",BizUtils.buildBizMessageLog(message));
        if (this.springBean == null) {
            this.springBean = SpringUtil.getBean(clazz);
        }
        String methodName = (String)message.getData().get("methodName");
        Method method = ReflectUtil.getMethodByName(this.clazz,methodName);
        if (method == null) {
            throw new BizException(BizResultEnum.APP_SERVICE_METHOD_NOT_FOUND,this.clazz.getName()+"的方法:" +methodName);
        }
        Object[] args = BizTools.convertJsonObject2MethodParameters(method,message.getData().get("params"),(JSONObject)message.getData().get("paramsTypes"));
        Object returnValue = null;
        try {
            returnValue = method.invoke(this.springBean,args);
        } catch (IllegalAccessException e) {
            log.error("执行方法出错",e);
            throw new BizException(BizResultEnum.OTHER_JAVA_CLASS_METHOD_ERROR,e);
        } catch (InvocationTargetException e) {
//            UndeclaredThrowableException e1;
//            e1.getUndeclaredThrowable()
//            java.lang.reflect.UndeclaredThrowableException
            Throwable t = e.getTargetException();
            if (t instanceof BizException) {
                throw (BizException) t;
            }
            else if (t instanceof UndeclaredThrowableException) {
                Throwable t1 = ((UndeclaredThrowableException) t).getUndeclaredThrowable();
                if (t1 instanceof BizException) {
                    throw (BizException) t1;
                }
            }

            log.error("执行方法出错",e);
            throw new BizException(BizResultEnum.OTHER_JAVA_CLASS_METHOD_ERROR,e);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.set("result", BizTools.methodReturnBean2Json(returnValue));
        BizMessage<JSONObject> result =  BizTools.buildJsonObjectMessage(message,jsonObject);
        log.debug("函数返回:\n{}",BizUtils.buildBizMessageLog(result));
        return result;
    }
}
