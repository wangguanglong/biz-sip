package com.bizmda.bizsip.app.api;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;

/**
 * integrator-bean-service服务抽象接口
 */
public interface IntegratorBeanInterface {
    /**
     * 执行聚合服务
     * @param message 传入的消息
     * @return 返回的消息
     */
    BizMessage<JSONObject> doBizService(BizMessage<JSONObject> message) throws BizException;
}
