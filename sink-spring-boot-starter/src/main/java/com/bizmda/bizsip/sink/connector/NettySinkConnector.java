package com.bizmda.bizsip.sink.connector;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import com.bizmda.bizsip.sink.connector.netty.NettyClient;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 史正烨
 */
@Slf4j
public class NettySinkConnector extends AbstractSinkConnector {
    private NettyClient nettyClient;

    @Override
    public void init(AbstractSinkConfig sinkConfig) throws BizException {
        super.init(sinkConfig);
        String host = (String) sinkConfig.getConnectorMap().get("host");
        Integer port = (Integer) sinkConfig.getConnectorMap().get("port");
        log.info("初始化NettySinkConnector:地址[{}]，端口[{}]",host,port);
        this.nettyClient = new NettyClient(host,port);
    }

    @Override
    public Object process(Object inMessage) throws BizException {
        log.debug("调用NettySinkConnector的process()");
//        log.trace("调用参数:\n{}", BizUtils.buildHexLog(inMessage));
        byte[] outMessage= (byte[])this.nettyClient.call((byte[])inMessage);
//        log.trace("返回结果:\n{}",BizUtils.buildHexLog((byte[])outMessage));
        return outMessage;
    }
}
