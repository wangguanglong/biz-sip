package com.bizmda.bizsip.sink.config;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import com.bizmda.bizsip.config.RabbitmqSinkConfig;
import com.bizmda.bizsip.config.RestSinkConfig;
import com.bizmda.bizsip.config.SinkConfigMapping;
import com.bizmda.bizsip.sink.processor.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 史正烨
 */
@Slf4j
@Configuration
public class SinkConfiguration {
    @Value("${bizsip.config-path:#{null}}")
    private String configPath;
    @Value("${bizsip.sink-id:#{null}}")
    private String sinkId;
    @Value("${bizsip.rabbitmq-log:#{null}}")
    private String rabbitmqLog;

    @Autowired
    private CachingConnectionFactory connectionFactory;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;
    private Map<String,DirectExchange> sinkExchangeMap;

    @Bean
    public SinkConfigMapping sinkConfigMapping() {
        try {
            SinkConfigMapping sinkConfigMapping = new SinkConfigMapping(this.configPath);
//            this.setHandlerMapping(sinkConfigMapping);
            return sinkConfigMapping;
        } catch (BizException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Bean
    @ConditionalOnProperty(name = "spring.rabbitmq.host",matchIfMissing = false)
    public RabbitTemplate rabbitTemplate(){
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
//        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
//
//        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
//            @Override
//            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
//                // TODO
//            }
//        });
//        rabbitTemplate.setReturnCallback(new RabbitTemplate.ReturnCallback() {
//            @Override
//            public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
//                log.debug("消息丢失:exchange({}),route({}),replyCode({}),replyText({}),message:{}",exchange,routingKey,replyCode,replyText,message);
//            }
//        });
        return rabbitTemplate;
    }

    @Bean
    public Map<String,DirectExchange> sinkExchangeMap(SinkConfigMapping sinkConfigMapping) {
        String[] sinkIds ;
        if (StrUtil.isEmpty(this.sinkId)) {
            sinkIds = new String[0];
        }
        else {
            sinkIds = this.sinkId.split(",");
        }
        this.sinkExchangeMap = new HashMap<String,DirectExchange>();
        for(String sinkId:sinkIds) {
            AbstractSinkConfig sinkConfig = sinkConfigMapping.getSinkConfigMap().get(sinkId);
            AbstractSinkProcessor sinkProcessor;
            try {
                switch(sinkConfig.getProcessor()) {
                    case AbstractSinkConfig.PROCESSOR_DEFAULT:
                        sinkProcessor = new SinkProcessor(sinkConfig);
                        break;
                    case AbstractSinkConfig.PROCESSOR_BEAN:
                        sinkProcessor = new BeanSinkProcessor(sinkConfig);
                        break;
                    case AbstractSinkConfig.PROCESSOR_SINK_BEAN:
                        sinkProcessor = new SinkBeanSinkProcessor(sinkConfig);
                        break;
                    default:
                        continue;
                }
            } catch (BizException e) {
                log.error("创建Sink处理器出错!",e);
                continue;
            }
            try {
                switch(sinkConfig.getType()) {
                    case AbstractSinkConfig.TYPE_REST:
                        this.regiestSinkController(sinkConfig,sinkProcessor);
                        break;
                    case AbstractSinkConfig.TYPE_RABBITMQ:
                        this.regiestSinkMessageListener(sinkConfig,sinkProcessor);
                        break;
                    default:
                        continue;
                }
            } catch (NoSuchMethodException | BizException e) {
                log.error("创建Sink处理器出错!",e);
                continue;
            }
        }
        return this.sinkExchangeMap;
    }


    private void regiestSinkController(AbstractSinkConfig sinkConfig,AbstractSinkProcessor sinkProcessor) throws NoSuchMethodException, BizException {
        if (!(sinkConfig instanceof RestSinkConfig)) {
            return;
        }
        RestSinkConfig restSinkConfig = (RestSinkConfig) sinkConfig;
        String path = URLUtil.getPath(restSinkConfig.getUrl());
        RequestMappingInfo info = RequestMappingInfo.paths(path).consumes("application/json").produces("application/json").methods(RequestMethod.POST).build();
        Method method = SinkRestController.class.getMethod("service", BizMessage.class);
        SinkRestController sinkRestController = new SinkRestController(sinkConfig);
        requestMappingHandlerMapping.registerMapping(info, sinkRestController, method);
        log.info("注册[rest,{}]类型[{}]接口:{}",
                AbstractSinkConfig.MESSAGE_PROCESSOR[sinkConfig.getProcessor()],
                sinkConfig.getId(),path);
    }


    private void regiestSinkMessageListener(AbstractSinkConfig sinkConfig,AbstractSinkProcessor sinkProcessor) throws BizException {
        ConfigurableApplicationContext context = (ConfigurableApplicationContext)applicationContext;
        RabbitmqSinkConfig rabbitmqSinkConfig = (RabbitmqSinkConfig)sinkConfig;
        SinkMessageListener sinkMessageListener = new SinkMessageListener(sinkConfig,rabbitTemplate(),this.rabbitmqLog);
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        Environment env = applicationContext.getEnvironment();
        container.setConcurrentConsumers(env.getProperty("spring.rabbitmq.listener.simple.concurrency",int.class));
        container.setMaxConcurrentConsumers(env.getProperty("spring.rabbitmq.listener.simple.max-concurrency",int.class));
        container.setPrefetchCount(env.getProperty("spring.rabbitmq.listener.simple.prefetch",int.class));
        Queue queue = new Queue(rabbitmqSinkConfig.getQueue(),true,false,false);
        context.getBeanFactory().registerSingleton("rabbitmq-queue-"+rabbitmqSinkConfig.getId(),queue);
        DirectExchange directExchange = this.sinkExchangeMap.get(rabbitmqSinkConfig.getExchange());
        if (directExchange == null) {
            directExchange = new DirectExchange(rabbitmqSinkConfig.getExchange(), true, false);
            context.getBeanFactory().registerSingleton("rabbitmq-exchange-"+rabbitmqSinkConfig.getExchange(),directExchange);
            this.sinkExchangeMap.put(rabbitmqSinkConfig.getExchange(),directExchange);
        }
        Binding binding = BindingBuilder.bind(queue).to(directExchange).with(rabbitmqSinkConfig.getRoutingKey());
        context.getBeanFactory().registerSingleton("rabbitmq-binding-"+rabbitmqSinkConfig.getId(),binding);
        container.setQueues(queue);
        container.setAcknowledgeMode(AcknowledgeMode.AUTO);
        container.setMessageListener(sinkMessageListener);
        context.getBeanFactory().registerSingleton("rabbitmq-listener-"+rabbitmqSinkConfig.getId(),container);

        log.info("注册[rabbitmq,{}]类型[{}]接口:queue（{}),exchange({}),key({})",
                AbstractSinkConfig.MESSAGE_PROCESSOR[sinkConfig.getProcessor()],
                rabbitmqSinkConfig.getId(),rabbitmqSinkConfig.getQueue(),
                rabbitmqSinkConfig.getExchange(),rabbitmqSinkConfig.getRoutingKey());
        return;
    }
}
