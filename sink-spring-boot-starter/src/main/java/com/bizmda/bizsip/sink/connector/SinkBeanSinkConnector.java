package com.bizmda.bizsip.sink.connector;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import com.bizmda.bizsip.sink.api.SinkBeanInterface;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 史正烨
 */
@Slf4j
@Getter
public class SinkBeanSinkConnector extends AbstractSinkConnector {
    private Object sinkBean;
    private String clazzName;

    @Override
    public void init(AbstractSinkConfig sinkConfig) throws BizException {
        super.init(sinkConfig);
        this.clazzName = (String) sinkConfig.getConnectorMap().get("class-name");
        log.info("初始化JavaSinkConnector:关联类[{}]",this.clazzName);
        try {
            sinkBean = SpringUtil.getBean(Class.forName(this.clazzName));
        } catch (ClassNotFoundException e) {
            log.error("获取类出错",e);
            throw new BizException(BizResultEnum.CONNECTOR_JAVA_CLASS_CREATE_ERROR,e);
        }
    }

    @Override
    public Object process(Object inMessage) throws BizException {
        log.debug("调用SinkBeanSinkConnector[{}]的process()",this.clazzName);
        if (sinkBean instanceof SinkBeanInterface) {
            JSONObject outMessage = ((SinkBeanInterface) sinkBean).process((JSONObject) inMessage);
            return outMessage;
        }
        else {
            throw new BizException(BizResultEnum.CONNECTOR_JAVA_CLASS_CREATE_ERROR,"不是SinkBeanInterface和JSONObjectSinkBeanInterface类型："+this.clazzName);
        }
   }
}
