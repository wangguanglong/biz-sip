package com.bizmda.bizsip.sink.connector;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import com.bizmda.bizsip.config.SinkConfigMapping;
import com.bizmda.bizsip.sink.api.SinkBeanInterface;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;

/**
 * 通讯适配器处理类
 */
@Slf4j
public class Connector {
    private AbstractSinkConnector sinkConnector;
    private boolean isJsonObject;

    /**
     * 根据sink-id获取通讯适配器调用接口
     * @param sinkId Sink层模块的sink-id
     * @return 通讯适配器调用接口
     */
    public static Connector getSinkConnector(String sinkId) {
        Connector connector = new Connector();
        SinkConfigMapping sinkConfigMapping = SpringUtil.getBean("sinkConfigMapping");
        AbstractSinkConfig sinkConfig = sinkConfigMapping.getSinkConfig(sinkId);
        try {
            if (sinkConfig == null) {
                throw new BizException(BizResultEnum.SINK_NOT_SET, "sinkId[" + sinkId + "]在sink.yml中没有配置");
            }
            if (sinkConfig.getConnectorMap() == null) {
                connector.sinkConnector = null;
                return connector;
            }
            String connectorType = (String) sinkConfig.getConnectorMap().get("type");

            Class<Object> clazz = (Class) AbstractSinkConnector.CONNECTOR_TYPE_MAP.get(connectorType);
            if (clazz == null) {
                throw new BizException(BizResultEnum.CONNECTOR_NOT_SET);
            }

            try {
                connector.sinkConnector = (AbstractSinkConnector) clazz.getDeclaredConstructor().newInstance();
                connector.sinkConnector.setType(connectorType);
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                throw new BizException(BizResultEnum.CONNECTOR_JAVA_CLASS_CREATE_ERROR, e);
            }

            connector.sinkConnector.init(sinkConfig);
            connector.isJsonObject = false;
            if ((connector.sinkConnector instanceof BeanSinkConnector)) {
                connector.isJsonObject = true;
            } else if (connector.sinkConnector instanceof SinkBeanSinkConnector) {
                if (((SinkBeanSinkConnector) connector.sinkConnector).getSinkBean() instanceof SinkBeanInterface) {
                    connector.isJsonObject = true;
                }
            }
        } catch (BizException e) {
            log.error("getSinkConnector("+sinkId+")出错!",e);
            return null;
        }
        return connector;
    }

    /**
     * 获取底层通讯适配器
     * @return 底层通讯适配器对象
     */
    public AbstractSinkConnector getSinkConnector() {
        return this.sinkConnector;
    }

    /**
     * 调用通讯适配器进行交互处理
     * @param inMessage 传入的消息报文字节流
     * @return 返回的消息报文字节流
     * @throws BizException Biz-SIP应用异常
     */
    public byte[] process(byte[] inMessage) throws BizException {
        if (isJsonObject) {
            throw new BizException(BizResultEnum.CONNECTOR_IN_PARAMETER_TYPE_ERROR,"传入byte[],但要求是JSONObject类型!");
        }
        return (byte[])this.sinkConnector.process(inMessage);
    }

    /**
     * 调用通讯适配器进行交互处理
     * @param inMessage 传入的消息报文（JSONObject类型）
     * @return 返回的消息报文（JSONObject类型）
     * @throws BizException Biz-SIP应用异常
     */
    @Deprecated
    public JSONObject process(JSONObject inMessage) throws BizException {
        if (!isJsonObject) {
            throw new BizException(BizResultEnum.CONNECTOR_IN_PARAMETER_TYPE_ERROR,"传入JSONObject,但要求是byte[]类型!");
        }
        return (JSONObject) this.sinkConnector.process(inMessage);
    }
}
