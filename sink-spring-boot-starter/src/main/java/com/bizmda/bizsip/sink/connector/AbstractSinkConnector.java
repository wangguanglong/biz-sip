package com.bizmda.bizsip.sink.connector;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 史正烨
 */
@Data
public abstract class AbstractSinkConnector {
    public static final Map<String,Object> CONNECTOR_TYPE_MAP = new HashMap<>();
    static {
        CONNECTOR_TYPE_MAP.put("sink-bean", SinkBeanSinkConnector.class);
        CONNECTOR_TYPE_MAP.put("netty", NettySinkConnector.class);
        CONNECTOR_TYPE_MAP.put("rabbitmq", RabbitmqSinkConnector.class);
        CONNECTOR_TYPE_MAP.put("bean", BeanSinkConnector.class);
    }

    private String type;

    /**
     * 协议适配处理模块的对外协议对接实现
     * @param packMessage
     * @return
     * @throws BizException
     */
    public abstract Object process(Object packMessage) throws BizException;

    public void init(AbstractSinkConfig sinkConfig) throws BizException {
    }
}
