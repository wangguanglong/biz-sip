package com.bizmda.bizsip.sink.processor;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import com.bizmda.bizsip.converter.Converter;
import com.bizmda.bizsip.sink.connector.Connector;

abstract public class AbstractSinkProcessor {
    protected AbstractSinkConfig sinkConfig;
    protected Converter converter = null;
    protected Connector connector = null;

    public AbstractSinkProcessor(AbstractSinkConfig sinkConfig) {
        this.sinkConfig = sinkConfig;
        this.converter = Converter.getSinkConverter(sinkConfig.getId());
        this.connector = Connector.getSinkConnector(sinkConfig.getId());
    }

    abstract JSONObject process(JSONObject inMessage) throws BizException;
}
