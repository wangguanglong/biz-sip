package com.bizmda.bizsip.config;

import cn.hutool.core.util.StrUtil;
import lombok.Data;

import java.util.*;

/**
 * @author 史正烨
 */
@Data
public class AbstractSinkConfig {
    public static final int TYPE_REST = 0;
    public static final int TYPE_RABBITMQ = 1;
    public static final String[] MESSAGE_TYPE = {"rest","rabbitmq"};
    public static final int PROCESSOR_DEFAULT = 0;
    public static final int PROCESSOR_SINK_BEAN = 1;
    public static final int PROCESSOR_BEAN = 2;
    public static final String[] MESSAGE_PROCESSOR = {"default","sink-bean","bean"};
    private String id;
    private int type;
    private int processor;
    private String className;

    private Map<String,Object> converterMap;
    private Map<String,Object> connectorMap;

    public AbstractSinkConfig(Map<String,Object> map) {
        this.id = (String)map.get("id");
        String type = (String)map.get("type");
        this.type = TYPE_REST;
        if ("rabbitmq".equalsIgnoreCase(type)) {
            this.type = TYPE_RABBITMQ;
        }
        String processor = (String)map.get("processor");
        this.processor = PROCESSOR_DEFAULT;
        if ("sink-bean".equalsIgnoreCase(processor)) {
            this.processor = PROCESSOR_SINK_BEAN;
        }
        else if ("bean".equalsIgnoreCase(processor)) {
            this.processor = PROCESSOR_BEAN;
        }
        this.className = (String) map.get("class-name");
        this.converterMap = (Map<String, Object>)map.get("converter");
        this.connectorMap = (Map<String, Object>)map.get("connector");
    }
}
